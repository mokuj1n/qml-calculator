import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2


ColumnLayout {
    id: cl_layout
    
    spacing: defMargin
    anchors.fill: parent
    anchors.margins: defMargin

    
    TextField {
        id: tf_calc_in
        objectName: "tf_calc_in"
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        font.pointSize: 18
        focus: true
        placeholderText: qsTr("0")
        horizontalAlignment: TextInput.AlignRight

        Keys.onReturnPressed: {
            solve()
        }
    } 

    Component {
        id: custom_button
        ButtonStyle {
            label: Component {
                Text {
                    text: control.text
                    clip: true
                    font.pointSize: 12
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                }
            }
        }
    }

    RowLayout {
        GridLayout {
            id: normal_grid
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            columnSpacing: 4
            rowSpacing: 4
            rows: 5
            columns: 4

            Button {
                id: btn_ac
                objectName: "btn_ac"
                text: qsTr("AC")
                Layout.fillHeight: true
                Layout.fillWidth: true
                tooltip: "Clears the calculator "
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_div
                objectName: "btn_div"
                text: qsTr("/")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_mult
                objectName: "btn_mult"
                text: qsTr("*")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_sub
                objectName: "btn_sub"
                text: qsTr("-")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_seven
                objectName: "btn_seven"
                text: qsTr("7")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_eight
                objectName: "btn_eight"
                text: qsTr("8")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_nine
                objectName: "btn_nine"
                text: qsTr("9")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_add
                objectName: "btn_add"
                text: qsTr("+")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_four
                objectName: "btn_four"
                text: qsTr("4")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_five
                objectName: "btn_five"
                text: qsTr("5")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_six
                objectName: "btn_six"
                text: qsTr("6")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_pow
                objectName: "btn_pow"
                text: qsTr("**")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_one
                objectName: "btn_one"
                text: qsTr("1")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_two
                objectName: "btn_two"
                text: qsTr("2")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_three
                objectName: "btn_three"
                text: qsTr("3")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_sqrt
                objectName: "btn_sqrt"
                text: qsTr("sqrt(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_brackets
                objectName: "btn_brackets"
                text: qsTr("(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_zero
                objectName: "btn_zero"
                text: qsTr("0")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_comma
                objectName: "btn_comma"
                text: qsTr(".")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_equals
                objectName: "btn_equals"
                text: qsTr("=")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: solve()
                style: custom_button
            }
        }

        ColumnLayout {
            id: extra_column
            visible: (appWindow.width > appWindow.minimumWidth + switchModeOffset) ? true : false

            Button {
                id: btn_sin
                objectName: "btn_sin"
                 text: qsTr("sin(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_cos
                objectName: "btn_cos"
                 text: qsTr("cos(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_tan
                objectName: "btn_tan"
                 text: qsTr("tan(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_tanh
                objectName: "btn_tanh"
                 text: qsTr("tanh(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }

            Button {
                id: btn_log
                objectName: "btn_log"
                 text: qsTr("log(..)")
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: update_display(text)
                style: custom_button
            }
        }
    }        
}
